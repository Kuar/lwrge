use cgmath::Vector3;
use cgmath::Matrix4;
use cgmath::Rad;
use cgmath::Point3;
use std::f32::consts::PI;
pub struct MirroredTransform {
    transform: Transform
}
impl MirroredTransform {
    pub fn new(position: (f32, f32, f32), rotation: (f32, f32, f32), scale: f32) -> MirroredTransform {
        MirroredTransform {
            transform: Transform::new(position, rotation, scale)
        }
    }
    pub fn set_rotation(&mut self, rotation: (f32, f32, f32)) {
        self.transform.set_rotation((-rotation.0, -rotation.1, -rotation.2));
    }
    pub fn set_position(&mut self, position: (f32, f32, f32)) {
        self.transform.set_position((-position.0, -position.1, -position.2));
    }
    pub fn rotate(&mut self, rotation: (f32, f32)) {
        self.transform.rotate((
            -rotation.1,
            -rotation.0,
            1.0 //Radians -- Means we are looking forwards
            ));
    }
    pub fn translate(&mut self, translation: (f32, f32, f32)) {
        self.transform.translate((
            translation.0,
            translation.1,
            translation.2
            ));
    }
    pub fn get_raw_rotation(&self) -> Vector3<f32> {
//        Vector3 {
//            x: (self.transform.rotation.x * PI) / 180.0,
//            y: (self.transform.rotation.y * PI) / 180.0,
//            z: (self.transform.rotation.z * PI) / 180.0}
        Vector3 {
            x: self.transform.rotation.x,
            y: self.transform.rotation.y,
            z: -1.0}
    }
    pub fn get_raw_position(&self) -> Point3<f32> {
        Point3::new(self.transform.position.x, self.transform.position.y, self.transform.position.z)
    }
    pub fn get_position(&self) -> &Matrix4<f32> {
        &self.transform.transform_matrix
    }
    pub fn get_rotation(&self) -> &Matrix4<f32> {
        &self.transform.rotation_matrix
    }
    pub fn get_scale(&self) -> &Matrix4<f32> {
        &self.transform.scale_matrix
    }
    pub fn transform(&self) -> [[f32; 4]; 4] {
        self.transform.transform()
    }
}

pub struct Transform {
    position: Vector3<f32>,
    rotation: Vector3<f32>,
    scale: f32,
    transform_matrix: Matrix4<f32>,
    rotation_matrix: Matrix4<f32>,
    scale_matrix: Matrix4<f32>
}

impl Transform {
    pub fn new(position: (f32, f32, f32), rotation: (f32, f32, f32), scale: f32) -> Transform {
        let position = Vector3::new(position.0, position.1, position.2);
        let rotation = Vector3::new(rotation.0, rotation.1, rotation.2);
        let transform_matrix = Matrix4::from_translation(Vector3::new(0.0, 0.0, 0.0));
        let rotation_matrix = Matrix4::from_angle_x(Rad(position.x));
        let scale_matrix = Matrix4::from_scale(scale);
        Transform {
            position,
            rotation,
            scale,
            transform_matrix,
            rotation_matrix,
            scale_matrix
        }
    }
    pub fn position(&self) -> (f32, f32, f32) {
        (self.position.x, self.position.y, self.position.z)
    }
    pub fn rotation(&self) -> (f32, f32, f32) {
        (self.rotation.x, self.rotation.y, self.rotation.z)
    }
    pub fn get_position(&self) -> &Matrix4<f32> {
        &self.transform_matrix
    }
    pub fn get_rotation(&self) -> &Matrix4<f32> {
        &self.rotation_matrix
    }
    pub fn get_scale(&self) -> &Matrix4<f32> {
        &self.scale_matrix
    }
    pub fn set_scale(&mut self, scale: f32) {
        self.scale = scale;
        self.scale_matrix = Matrix4::from_scale(self.scale)
    }
    pub fn set_position(&mut self, position: (f32, f32, f32)) {
        self.position.x = position.0;
        self.position.y = position.1;
        self.position.z = position.2;
        self.transform_matrix = Matrix4::from_translation(self.position);
    }
    pub fn translate(&mut self, translation: (f32, f32, f32)) {
            let pos = self.position;
            self.set_position((
                pos.x + translation.0,
                pos.y + translation.1,
                pos.z + translation.2
            ))
    }
    pub fn rotate(&mut self, rotation: (f32, f32, f32)) {
        let rot = self.rotation;
        self.set_rotation((
            rot.x + rotation.0,
            rot.y + rotation.1,
            rot.z + rotation.2
            )
        )
    }
    pub fn set_rotation(&mut self, rotation: (f32, f32, f32)) {
        self.rotation.x = rotation.0;
        self.rotation.y = rotation.1;
        self.rotation.z = rotation.2;
        self.rotation_matrix =
            Matrix4::from_angle_x(Rad(self.rotation.x))
            * Matrix4::from_angle_y(Rad(self.rotation.y))
            * Matrix4::from_angle_z(Rad(self.rotation.z));
    }
    pub fn transform(&self) -> [[f32; 4]; 4] {      //Construct an [[f32; 4]; 4] from a Matrix4
        let m =  self.transform_matrix * self.rotation_matrix * self.scale_matrix;
        [
            [m.x.x, m.x.y, m.x.z, m.x.w],
            [m.y.x, m.y.y, m.y.z, m.y.w],
            [m.z.x, m.z.y, m.z.z, m.z.w],
            [m.w.x, m.w.y, m.w.z, m.w.w]
        ]
    }
}