use transform;
use cgmath::{Matrix4, Vector3, Point3};
use glium;
use glium::Surface;
use cgmath::Rad;
use cgmath::Angle;
use std::f32::consts::PI;

pub struct Camera {
    transform: transform::MirroredTransform
}

impl Camera {
    pub fn new() -> Camera {
        Camera {
            transform: transform::MirroredTransform::new((0.0, 0.0, 0.0), (0.0, 0.0, 0.0), 1.0)
        }
    }
    pub fn update(&mut self, events: Option<&glium::glutin::VirtualKeyCode>) {
        match events {
            Some(glium::glutin::VirtualKeyCode::Left) => {
                self.transform().rotate((-1.8, 0.0));
            },
            Some(glium::glutin::VirtualKeyCode::Right) => {
                self.transform().rotate((1.8, 0.0));
            },
            Some(glium::glutin::VirtualKeyCode::Up) => {
              self.transform().translate((0.0, 0.0, 0.005));
            },
            Some(glium::glutin::VirtualKeyCode::Down) => {
                self.transform().translate((0.0, 0.0, -0.005));
            },
            Some(glium::glutin::VirtualKeyCode::Q) => {
                self.transform().translate((-0.005, 0.0, 0.0));
            },
            Some(glium::glutin::VirtualKeyCode::E) => {
                self.transform().translate((0.005, 0.0, 0.0));
            },
            _ => {}
        }
    }
    pub fn orientation(&mut self) -> [[f32; 4]; 4] {
        let mut rot = self.ref_transform().get_raw_rotation();
        let mut pos = self.ref_transform().get_raw_position();
//        println!("{:?} : {:?}", rot.y.cos(), rot.y.sin()); //debug fmt
        rot.y = (rot.y / 180.0) * PI;
        let lookdir = Point3 {x: rot.y.cos() + pos.x, y: 0.0 + pos.y, z: rot.y.sin() + pos.z};
        let m = Matrix4::look_at(
            pos,
            lookdir,
            Vector3 {x: 0.0, y: 1.0, z: 0.0});
        [
            [m.x.x, m.x.y, m.x.z, m.x.w],
            [m.y.x, m.y.y, m.y.z, m.y.w],
            [m.z.x, m.z.y, m.z.z, m.z.w],
            [m.w.x, m.w.y, m.w.z, m.w.w]
        ]
    }
//    ,' ``',
//    '  (o)(o)
//    `       > ;
//    ',     . ...-'"""""`'.
//    .'`',`''''`________:   ":
//   (`'. '.;  |           ;/\;\;
//  (`',.',.;  |               |
// (,'` .`.,'  |               |        Me throwing laptop at the floor trying to understand
// (,.',.','   |               |        math wizardry involved with LookAt
//(,.',.-`_____|               |
//    __\_ _\_ |               |
//             |_______________|
    pub fn ref_transform(&self) -> &transform::MirroredTransform { &self.transform }
    pub fn transform(&mut self) -> &mut transform::MirroredTransform {
        &mut self.transform
    }
    pub fn perspective(&self, target: glium::Frame) -> ([[f32; 4]; 4], glium::Frame) {
        let (width, height) = target.get_dimensions();
        let aspect_ratio = height as f32 / width as f32;

        let fov: f32 = 3.141592 / 3.0;
        let zfar = 1024.0;
        let znear = 0.1;

        let f = 1.0 / (fov / 2.0).tan();
        (
            [
                [f *   aspect_ratio   ,    0.0,              0.0              ,   0.0],
                [         0.0         ,     f ,              0.0              ,   0.0],
                [         0.0         ,    0.0,  (zfar+znear)/(zfar-znear)    ,   1.0],
                [         0.0         ,    0.0, -(2.0*zfar*znear)/(zfar-znear),   0.0],
            ],
            target
        )
    }
}