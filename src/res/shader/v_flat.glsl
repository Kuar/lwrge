    #version 150

    in vec3 position;
    in vec3 normal;
    in vec2 tex_coord;
    out vec3 v_normal;
    out vec2 v_tex_coords;
    uniform mat4 model; //Passed from world game_loop as [[f32; 4]; 4] from position * scale * rotation
    uniform mat4 view;
    uniform mat4 perspective;
    void main() {
        v_normal = transpose(inverse(mat3(model))) * normal;
        v_tex_coords = tex_coord;
        gl_Position = perspective * view * model * vec4(position, 1.0);
    }