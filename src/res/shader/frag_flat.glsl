#version 140

in vec3 v_normal;
in vec2 v_tex_coords;
out vec4 color;
uniform vec3 u_light;
uniform sampler2D tex;

void main() {
    float brightness = dot(normalize(v_normal), normalize(u_light));
    color = texture(tex, v_tex_coords) * (brightness + vec4(0.5, 0.5, 0.5, 0.0));
}