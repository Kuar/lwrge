pub const V_FLAT_SHADER: &str = include_str!("./shader/v_flat.glsl");
pub const FRAG_FLAT_SHADER: &str = include_str!("./shader/frag_flat.glsl");
pub const CUBE: &str = "src/res/mesh/cube.obj";
pub const DEER: &str = "src/res/mesh/CorruptedDeer.obj";
pub const WINDMILL: &str = "src/res/mesh/windmill.obj";