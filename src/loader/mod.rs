use res;
use mesh;
use glium;
use tobj;
use image;
use std::path::Path;
use std::io::Cursor;
use std::rc::Rc;
use std::io::{BufRead, Seek};

#[derive(Debug)]
pub struct MeshData {
    vertices: Vec<mesh::Vertex>,
    vertex_buffer: glium::VertexBuffer<mesh::Vertex>,
    normals: Vec<mesh::Normal>,
    normal_buffer: glium::VertexBuffer<mesh::Normal>,
    indices: glium::IndexBuffer<u16>,
    tex_coords: glium::VertexBuffer<mesh::TextureCoord>,
    shader: glium::Program,
    texture: Rc<glium::texture::Texture2d>
}
impl MeshData {
    pub fn new(display: &glium::Display, file: &Path, texture: Rc<glium::texture::Texture2d>, v_shader: &str, f_shader: &str) -> MeshData {
        let mut vertices = Vec::new();
        let mut normals = Vec::new();
        let mut indices = Vec::new();
        let mut tex_coords = Vec::new();
        let obj = tobj::load_obj(file);
        assert!(obj.is_ok());
        let (model, material) = obj.unwrap();
        for (i, m) in model.iter().enumerate() {
            let mesh = &m.mesh;
            for v in mesh.positions.chunks(3) {
                vertices.push(mesh::Vertex { position: (v[0], v[1], v[2])});
            }
            for n in mesh.normals.chunks(3) {
                normals.push(mesh::Normal { normal: (n[0], n[1], n[2])});
            }
            for i in &mesh.indices {
                indices.push(*i as u16);
            }
            for t in mesh.texcoords.chunks(2) {
                //println!("{:?}", t);
                tex_coords.push( mesh::TextureCoord { tex_coord: (t[0], t[1]) });
            }
        }
        let vbo = glium::VertexBuffer::new(display, &vertices).unwrap();
        let ibo = glium::IndexBuffer::new(display, glium::index::PrimitiveType::TrianglesList, &indices).unwrap();
        let nbo = glium::VertexBuffer::new(display, &normals).unwrap();
        let tcb = glium::VertexBuffer::new(display, &tex_coords).unwrap();
        let shader = glium::Program::from_source(display, v_shader, f_shader, None).unwrap();
        MeshData {
            vertex_buffer: vbo,
            normal_buffer: nbo,
            tex_coords: tcb,
            vertices,
            normals,
            indices: ibo,
            shader,
            texture: texture
        }
    }
    pub fn vertices(&self) -> &Vec<mesh::Vertex> {
        &self.vertices
    }
    pub fn vertices_mut(&mut self) -> &mut Vec<mesh::Vertex> {
        &mut self.vertices
    }
    pub fn normals(&self) -> &Vec<mesh::Normal> {
        &self.normals
    }
    pub fn texture(&self) -> &glium::texture::Texture2d { &self.texture }
    pub fn indices(&self) -> &glium::IndexBuffer<u16> {
        &self.indices
    }
    pub fn vertex_buffer(&self) -> &glium::VertexBuffer<mesh::Vertex> {
        &self.vertex_buffer
    }
    pub fn normal_buffer(&self) -> &glium::VertexBuffer<mesh::Normal> {
        &self.normal_buffer
    }
    pub fn tex_coords(&self) -> &glium::VertexBuffer<mesh::TextureCoord> { &self.tex_coords }
    pub fn program(&self) -> &glium::Program {
        &self.shader
    }
}

pub fn load_texture<T: BufRead+Seek>(display: &glium::Display, image: T) -> glium::texture::Texture2d {
    let image = image::load(image,
                            image::JPEG).unwrap().to_rgba();
    let image_dimensions = image.dimensions();
    let image = glium::texture::RawImage2d::from_raw_rgba_reversed(&image.into_raw(), image_dimensions);
    glium::texture::Texture2d::new(display, image).unwrap()
}