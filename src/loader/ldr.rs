use res;
use mesh;
use glium;

pub struct MeshData {
    vertices: Vec<mesh::Vertex>,
    vertex_buffer: u16,
    normals: Vec<mesh::Normal>,
    normal_buffer: u16,
    indices: Vec<u16>,
    tex_coords: u16,
}
impl MeshData {
    pub fn new(file: &str) -> MeshData {
        let mut vertices = Vec::new();
        let mut normals = Vec::new();
        let mut indices = Vec::new();
        vertices.push(mesh::Vertex{position: (0.0, 0.0, 0.0)});
        normals.push(mesh::Normal{position: (0.0, 0.0, 0.0)});
        indices.push(0u16);
        for line in file.lines() {
            let mut split_line = line.split_whitespace();
            macro_rules! next {
                () => { split_line.next().unwrap().parse().unwrap() };
            }
            match split_line.next().unwrap() {
                "v" => {
                    vertices.push(mesh::Vertex {
                        position: (next!(), next!(), next!())
                    })
                },
                "vn" => {
                    normals.push(mesh::Normal {
                        position: (next!(), next!(), next!())
                    })
                },
                "f" => {
                    for mut indice in split_line.into_iter() {
                        indices.push(
                            indice.split("/").next().unwrap().parse().unwrap()
                        )
                    }
                },
                _ => {}
            }
        }
        MeshData {
            vertex_buffer: 0,
            normal_buffer: 0,
            tex_coords: 0,
            vertices,
            normals,
            indices,
        }
    }
    pub fn vertices(&self) -> &Vec<mesh::Vertex> {
        &self.vertices
    }
    pub fn normals(&self) -> &Vec<mesh::Normal> {
        &self.normals
    }
    pub fn indices(&self) -> &Vec<u16> {
        &self.indices
    }
}