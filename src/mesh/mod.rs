use loader;
use glium;
use std::path::Path;
use std::rc::Rc;

pub struct Mesh {
    data: loader::MeshData,
}
#[derive(Copy, Clone, Debug)]
pub struct Vertex {
    pub position: (f32, f32, f32)
}
#[derive(Copy, Clone, Debug)]
pub struct Normal {
    pub normal: (f32, f32, f32)
}
#[derive(Copy, Clone, Debug)]
pub struct TextureCoord {
    pub tex_coord: (f32, f32)
}
implement_vertex!(TextureCoord, tex_coord);
implement_vertex!(Vertex, position);
implement_vertex!(Normal, normal);

impl Mesh {
    pub fn new(display: &glium::Display, object: &Path, texture: Rc<glium::texture::Texture2d>, v_shader: &str, f_shader: &str) -> Mesh {
        Mesh {
            data: loader::MeshData::new(display,object, texture, v_shader, f_shader)
        }
    }
    pub fn data(&self) -> &loader::MeshData {
        &self.data
    }
}