use gameobject;
use gameobject::Logic;
use glium;
use mesh;
use camera;
use glium::glutin;
use glium::Surface;
use rayon::prelude::*;

pub struct World {
    objects: Vec<Box<dyn gameobject::Logic>>,
    camera: camera::Camera,
    display: glium::Display,
    events: glutin::EventsLoop,
}
unsafe impl Send for World {}
impl World {
    pub fn new() -> World {
        let events_loop = glutin::EventsLoop::new();
        let window = glutin::WindowBuilder::new()
            .with_dimensions(glutin::dpi::LogicalSize::new(512.0, 512.0))
            .with_title("lwrge");
        let context = glutin::ContextBuilder::new()
            .with_depth_buffer(24)
            .with_vsync(true);
        let display = glium::Display::new(window, context, &events_loop).unwrap();
        World {
            objects: Vec::new(),
            camera: camera::Camera::new(),
            display,
            events: events_loop
        }
    }
    pub fn display(&self) -> &glium::Display {
        &self.display
    }
    pub fn camera(&mut self) -> &mut camera::Camera { &mut self.camera }
    pub fn objects(&self) -> &Vec<Box<dyn Logic>> {
        &self.objects
    }
    pub fn add_object<O: Logic + 'static>(&mut self, obj: O) {
        self.objects.push(Box::new(obj));
    }
    pub fn game_loop(&mut self) {
        let mut closed = false;
        let light = [0.5, 0.0, -1.0f32];
        let mut input: (Option<glutin::VirtualKeyCode>, glutin::ElementState) = (None, glutin::ElementState::Released);
        let params = glium::DrawParameters {
            depth: glium::Depth {
                test: glium::DepthTest::IfLess,
                write: true,
                .. Default::default()
            },
            backface_culling: glium::draw_parameters::BackfaceCullingMode::CullCounterClockwise,
            .. Default::default()
        };
        while !closed {
            self.camera.update(input.0.as_ref());
            self.objects.par_iter_mut().for_each(|o| o.update(input.0.as_ref()));
            if input.1 == glutin::ElementState::Released {
                input.0 = None;
            }
            let (perspective, mut target) = self.camera.perspective(self.display.draw());
            target.clear_color_and_depth((0.0, 0.0, 1.0, 1.0), 1.0);
            for obj in &mut self.objects {
          //      obj.update();
                if (obj.ref_transform().position().2 <= 35.0) & (obj.ref_transform().position().2 >= -20.0) &
                    (obj.ref_transform().position().1 <= 20.0 ) & (obj.ref_transform().position().1 >= -20.0) &
                    (obj.ref_transform().position().0 <= 15.0) & (obj.ref_transform().position().0 >= -15.0) {
                    let uniforms = uniform! {
                        model: obj.ref_transform().transform(),
                        view: self.camera.orientation(),
                        perspective: perspective,
                        u_light: light,
                        tex: obj.mesh().data().texture()
                   };
                    target.draw(
                        (obj.mesh().data().vertex_buffer(),
                         obj.mesh().data().normal_buffer(),
                         obj.mesh().data().tex_coords()
                        ),
                        obj.mesh().data().indices(),
                        obj.mesh().data().program(),
                        &uniforms,
                        &params).unwrap();
                }
            }
            self.objects.retain(|o| o.is_alive());
            target.finish().unwrap();

            self.events.poll_events(|ev| {
                match ev {
                    glutin::Event::WindowEvent { event, .. } => match event {
                        glutin::WindowEvent::CloseRequested => closed = true,
                        _ => (),
                    },
                    glutin::Event::DeviceEvent { event, .. } => match event {
                        glutin::DeviceEvent::Key(key) => input = (key.virtual_keycode, key.state),
                        _ => (),
                    }
                    _ => (),
                }
            });
        }
    }
}