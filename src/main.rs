#![allow(dead_code)]
#[macro_use]
extern crate glium;
extern crate cgmath;
extern crate hlua;
extern crate tobj;
extern crate image;
extern crate rayon;

mod camera;
mod component;
mod gameobject;
mod loader;
mod mesh;
mod renderer;
mod shader;
mod res;
mod transform;
mod world;
use std::path::Path;
use gameobject::Logic;
use std::rc::Rc;
use std::io::Cursor;
use std::io::BufReader;

fn main() {
    let mut world = world::World::new();

    let text = Rc::new(loader::load_texture(
        world.display(),
        Cursor::new(&include_bytes!("./res/mesh/dirt3.jpg")[..])
    ));
    let deer_tex = Rc::new(loader::load_texture(
        world.display(),
        Cursor::new(&include_bytes!("./res/mesh/deer.jpg")[..])
    ));
    let w_tex = Rc::new(loader::load_texture(
        world.display(),
        Cursor::new(&include_bytes!("./res/mesh/Windmill.jpg")[..])
    ));
    let deer = Rc::new(
        mesh::Mesh::new(
            world.display(),
            Path::new(res::DEER),
            deer_tex.clone(),
            res::V_FLAT_SHADER,
            res::FRAG_FLAT_SHADER
        )
    );
    let cube = Rc::new(
        mesh::Mesh::new(world.display(),
        Path::new(res::CUBE),
        text.clone(),
        res::V_FLAT_SHADER,
        res::FRAG_FLAT_SHADER)
    );
    let windmill = Rc::new(
        mesh::Mesh::new(world.display(),
                        Path::new(res::WINDMILL),
                        w_tex.clone(),
                        res::V_FLAT_SHADER,
                        res::FRAG_FLAT_SHADER)
    );

    for x in 0..1 {
        let mut _cube =gameobject::Cube::new(0,cube.clone());
        _cube.transform().set_position((0.0, 0.0, 3.0));
        _cube.transform().set_scale(0.05);
        //cube.transform().rotate((0.0, -20.0, 0.0));
        world.add_object(_cube);
    }
//    let mut _cube = gameobject::Cube::new(0,cube.clone());
//    _cube.transform().set_position((2.0, 0.0, 5.75));
//    _cube.transform().set_scale(0.3);
//    world.add_object(_cube);
//    let mut _deer = gameobject::Cube::new(1,deer.clone());
//    _deer.transform().set_position((0.0, -0.5, 3.75));
//    _deer.transform().set_scale(0.3);
//    world.add_object(_deer);
//    let mut _wmill = Box::new(gameobject::Cube::new(windmill.clone()));
//    _wmill.transform().set_position((-2.0, -0.5, 3.75));
//    _wmill.transform().set_scale(0.3);
//    world.add_object(_wmill);
    world.camera().transform().set_position((0.0, 0.3, 1.5));
    world.camera().transform().set_rotation((0.0, 90.0, 0.0));
   // world.add_object(cube);
    //world.camera().transform().set_position((1.0, 0.0, 0.0));
   world.game_loop();
}


//World // Core Game Loop
//Loader
//Mesh
//Shaders
//Transform
//Camera
//GameObject
//Renderer
//LUA handler