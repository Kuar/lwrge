use mesh;
use glium;
use std::path::Path;
use transform;
use std::rc::Rc;

pub struct GameObject {
    channel: u8,
    mesh: Rc<mesh::Mesh>,
    transform: transform::Transform
}

pub trait Logic: Send+Sync {
    fn update(&mut self, event: Option<&glium::glutin::VirtualKeyCode>);
    fn is_alive(&self) -> bool;
    fn mesh(&self) -> &mesh::Mesh;
    fn gameobject(&mut self) -> &mut GameObject;
    fn gameobject_ref(&self) -> &GameObject;
    fn transform(&mut self) -> &mut transform::Transform;
    fn ref_transform(&self) -> &transform::Transform;
}

pub struct Cube {
    object: GameObject,
    speed: f32,
    counter: u16
}
impl Cube {
    pub fn new(channel: u8, mesh: Rc<mesh::Mesh>) -> Cube {
        Cube {
            object: GameObject::new(channel, mesh),
            speed: 0.006,
            counter: 0
        }
    }
}
unsafe impl Send for Cube {}
unsafe impl Sync for Cube {}
impl Logic for Cube {
    fn update(&mut self, events: Option<&glium::glutin::VirtualKeyCode>) {
        let speed = self.speed;
        self.gameobject().transform().rotate((0.0, speed / 5.0, 0.0));
        match events {
            Some(glium::glutin::VirtualKeyCode::D) => {
                self.gameobject().transform().translate((speed, 0.0, 0.0));
            },
            Some(glium::glutin::VirtualKeyCode::A) => {
                self.gameobject().transform().translate((-speed, 0.0, 0.0));
            },
            Some(glium::glutin::VirtualKeyCode::S) => {
                self.gameobject().transform().translate((0.0, 0.0, -speed));
            },
            Some(glium::glutin::VirtualKeyCode::W) => {
                self.gameobject().transform().translate((0.0, 0.0, speed));
            },
            _ => {}
        }
//        let speed = self.speed;
//        self.counter += 1;
//        self.gameobject().transform().rotate((0.0, speed * 16.0, 0.0));
//        //println!("{:#?}", self.gameobject().transform().get_rotation());
//        self.gameobject().transform().translate((speed * 55.0, 0.0, speed * 50.0));
//        if self.gameobject().transform().position().0 <=-1.0 || self.gameobject().transform().position().0 >= 15.0 {
//            self.speed = -self.speed;
//        }
    }
    fn mesh(&self) -> &mesh::Mesh {
        &self.object.mesh()
    }
    fn is_alive(&self) -> bool {
        true
    }
    fn gameobject(&mut self) -> &mut GameObject {
        &mut self.object
    }
    fn gameobject_ref(&self) -> &GameObject { &self.object }
    fn transform(&mut self) -> &mut transform::Transform {
        self.gameobject().transform()
    }
    fn ref_transform(&self) -> &transform::Transform {
        &self.object.transform
    }
}

impl GameObject {
//    pub fn new(display: &glium::Display, path: &Path, texture: Rc<glium::texture::Texture2d>, v_shader: &str, f_shader: &str) -> GameObject {
//        GameObject {
//            mesh: mesh::Mesh::new(display, path, texture, v_shader, f_shader),
//            transform: transform::Transform::new((0.0, 0.0, 0.0), (0.0, 0.0, 0.0), 1.0)
//        }
//    }
    pub fn new(channel: u8, mesh: Rc<mesh::Mesh>) -> GameObject {
        GameObject {
            channel,
            mesh,
            transform: transform::Transform::new((0.0, 0.0, 0.0), (0.0, 0.0, 0.0), 1.0)
        }
    }
    pub fn channel(&self) -> &u8 {
        &self.channel
    }
    pub fn mesh(&self) -> &mesh::Mesh {
        &self.mesh
    }
    pub fn transform(&mut self) -> &mut transform::Transform {
        &mut self.transform
    }
}